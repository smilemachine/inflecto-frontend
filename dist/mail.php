<?php

header("Cache-Control: no-cache, must-revalidate");
header("Content-Type: application/json");

$success = false;
$message = null;
$data = [];

$requiredFields = [
  "check" => "Something went wrong. Please try again.",
  "fname" => "Your first name is required.",
  "lname" => "Your last name is required.",
  "company" => "Your company name is required.",
  "email" => "A valid email address is required.",
  "telephone" => "Your telephone number is required.",
  "message" => "Please write a message.",
  "start" => "That was a bit fast wasn't it? Please try again."
];

foreach ($requiredFields as $requiredField => $errorMessage) {
  if (!isset($_POST[$requiredField]) || empty(trim($_POST[$requiredField]))) {
    $success = false;
    $message = $errorMessage;
    break;
  }

  $value = trim($_POST[$requiredField]);

  /**
   * Do any further validation
   */
  switch ($requiredField) {
    case "fname":
      $data["First Name"] = $value;
      break;
    case "lname":
      $data["Last Name"] = $value;
      break;
    case "company":
      $data["Company"] = $value;
      break;
    case "email":
      if (filter_var($value, FILTER_VALIDATE_EMAIL) !== false) {
        $data["Email"] = $value;
      } else {
        $message = $errorMessage;
        break(2);
      }
      break;
    case "telephone":
      $data["Telephone"] = $value;
      break;
    case "message":
      $data["Message"] = $value;
      break;

    /**
     * This will be set automatically to 1 via javascript when the user
     * enters the "contact" section on the website
     */
    case "check":
      if (intval($value) !== 1) {
        $message = $errorMessage;
        break(2);
      }
      break;

    /**
     * It"s a bit suspicious if the user fills out the form in less than 5 seconds
     */
    case "start":
      if (strtotime("now") < $value + 5) {
        $message = $errorMessage;
        break(2);
      }
      break;
    default:
      break;
  }
}

/**
 * If the data array has content in it, we can send the email
 */
if (!empty($data) && is_null($message)) {
  require_once dirname(__FILE__) . '/../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

  $to = "info@inflectomedia.com";
  $from = $data["Email"];
  $subject = "Inflecto Website Contact Form";
  $body = "Hello,\n\nYou have received a message from the Inflecto website. Details are below.\n\n";

  foreach ($data as $key => $value) {
    $body .= sprintf("%20s: %s\n", $key, $value);
  }

  $body .= "\nRegards,\n\nInflecto Media";

  $mail = new PHPMailer;
  $mail->isSMTP();
  $mail->SMTPDebug = 0;
  $mail->Host = "smtp.gmail.com";
  $mail->Port = 587;
  $mail->SMTPSecure = "tls";
  $mail->SMTPAuth = true;
  $mail->Username = "";
  $mail->Password = "";
  $mail->SetFrom($from);
  $mail->AddAddress($to);
  $mail->Subject = $subject;
  $mail->Body = $body;
  $sent = $mail->Send();

  if ($sent === true) {
    $success = true;
    $message = "Thanks! We'll be in touch shortly.";
  } else {
    $success = false;
    $message = "Sorry, something went wrong. Please try again.";
  }
}

echo(json_encode([
  "success" => $success,
  "message" => $message
]));
