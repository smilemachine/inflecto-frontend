'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var scss = require('gulp-scss');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

var paths = {
  images: [],
  scripts: [
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/jquery-validation/dist/jquery.validate.min.js',
    './node_modules/jquery-easing/dist/jquery.easing.1.3.umd.min.js',
    './node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
    './node_modules/scrollmagic/scrollmagic/minified/plugins/jQuery.ScrollMagic.min.js',
    './node_modules/slick-carousel/slick/slick.min.js',
    './src/assets/js/mousewheel-smooth-scroll.js',
    './src/assets/js/inflecto.js'
  ],
  homeScripts: [
    './src/assets/js/home-section-*.js',
    './src/assets/js/site.js'
  ],
  privacyScripts: [
    './src/assets/js/site.js',
    './src/assets/js/privacy-page.js'
  ],
  scss: ['./src/assets/scss/style.scss'],
  fonts: ['./node_modules/font-awesome/fonts/*']
}

gulp.task('fonts', function () {
  return gulp.src(paths.fonts)
    .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('scss', function () {
  return gulp.src(paths.scss)
    .pipe(scss())
    .pipe(gulp.dest('dist/assets/css'));
});

gulp.task('scripts', function () {
  return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('homeScripts', function () {
  return gulp.src(paths.homeScripts)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('scripts-home.min.js'))
    .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('privacyScripts', function () {
  return gulp.src(paths.privacyScripts)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('scripts-privacy.min.js'))
    .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('default', ['scripts', 'homeScripts', 'privacyScripts', 'scss', 'fonts']);
