var inflectoSectionOffset = Number($(window).height() < 700 ? 150 : 350);
var inflecto = {

  /**
   * Set up the ScrollMagic controller
   */
  controller: new ScrollMagic.Controller({
    vertical: true,
    loglevel: 3,
    globalSceneOptions: {
      triggerHook: "onEnter",
      offset: inflectoSectionOffset,
      reverse: false
    }
  }),


  /**
   * Returns the window"s current height
   *
   * @return float
   */
  getWindowHeight: function () {
    return $(window).height();
  },


  /**
   * Sets the height of all page sections
   *
   * @return void
   */
  setSectionHeights: function () {
    $("section, #main > .scrollmagic-pin-spacer").css({
      minHeight: this.getWindowHeight(),
      height: 'auto'
    });
  },


  /**
   * Sets the width of the main container
   *
   * @return void
   */
  setMainWidth: function () {
    $("#main").css({
      width: $(window).width()
    });
  },


  /**
   * Sets the height of the menu
   *
   * Bit of a hack to prevent coloured bars appearing when scrolling on
   * mobile devices. The browser's toolbar disapears, adding an extra
   * few pixels to the window's height.
   *
   * @return void
   */
  setNavOpenHeights: function () {
    $("#nav").css({
      height: $(window).height() + 200
    });
  },


  /**
   * Scroll to a given element
   */
  scrollToElement: function (element) {
    $("html, body").animate({
			scrollTop: element.position().top
		}, 1000, "easeInOutQuart");
  },


  /**
   * Updates an element colour depending on where we are on the page
   */
  setElementColor: function (element) {
    var elementTop = element.offset().top;
    var triggerY = elementTop;

    $("section").each(function () {
      var sectionTop = $(this).position().top;
      var sectionBottom = sectionTop + $(this).outerHeight();

      if (triggerY >= sectionTop && triggerY <= sectionBottom) {
        if ($(this).hasClass("section-light")) {
          element.removeClass("section-dark");
          element.addClass("section-light");
        } else {
          element.removeClass("section-light");
          element.addClass("section-dark");
        }

        return false;
      }
    });
  },


  /**
   * Set back-to-top visibility
   */
  setBackToTopVisibility: function () {
    var scrollTop = $(window).scrollTop();
    var triggerHeight = $(window).height();

    if ($("body").hasClass("privacy") && triggerHeight < 500) {
      triggerHeight = 500;
    }

    if (scrollTop >= triggerHeight) {
      if (!$("#back-to-top").hasClass("make-visible")) {
        $("#back-to-top").addClass("make-visible");
      }
    } else {
      $("#back-to-top").removeClass("make-visible");
    }
  },


  /**
   * Sections aren't being triggered on mobile devices
   */
  triggerSections: function () {
    var currentTop = $(document).scrollTop();
    var currentBot = currentTop + $(window).height();

    $("section").each(function () {
      var sectionTop = $(this).position().top;

      if (currentBot - inflectoSectionOffset >= sectionTop) {
        $(this).removeClass("section-initial");
      }
    });
  },


  /**
   * Find us map
   */
  mapConfig: {
    center: {lat: 51.5180440, lng: -0.1338600},
    zoom: 16,
    zoomControl: false,
    mapTypeControl: false,
    streetViewControl: false,
    styles: [
      {"elementType": "geometry","stylers": [{"color": "#212121"}]}, {"elementType": "labels.icon","stylers": [{"visibility": "off"}]}, {"elementType": "labels.text.fill","stylers": [{"color": "#757575"}]}, {"elementType": "labels.text.stroke","stylers": [{"color": "#212121"}]}, {"featureType": "administrative","elementType": "geometry","stylers": [{"color": "#757575"}]}, {"featureType": "administrative.country","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]}, {"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#bdbdbd"}]}, {"featureType": "landscape.man_made","stylers": [{"color": "#000000"}]}, {"featureType": "landscape.natural","stylers": [{"color": "#4d4d4d"}, {"visibility": "on"}]}, {"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]}, {"featureType": "poi.attraction","stylers": [{"color": "#6b6b6b"}, {"visibility": "off"}]}, {"featureType": "poi.park","stylers": [{"color": "#131313"}, {"visibility": "on"}]}, {"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#181818"}]}, {"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#404040"}]}, {"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#616161"}]}, {"featureType": "poi.park","elementType": "labels.text.stroke","stylers": [{"color": "#1b1b1b"}]}, {"featureType": "road","elementType": "geometry.fill","stylers": [{"color": "#2c2c2c"}]}, {"featureType": "road","elementType": "labels.text.fill","stylers": [{"color": "#8a8a8a"}]}, {"featureType": "road.arterial","elementType": "geometry","stylers": [{"color": "#373737"}]}, {"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#3c3c3c"}]}, {"featureType": "road.highway.controlled_access","elementType": "geometry","stylers": [{"color": "#4e4e4e"}]}, {"featureType": "road.local","elementType": "labels.text.fill","stylers": [{"color": "#616161"}]}, {"featureType": "transit","stylers": [{"visibility": "on"}]}, {"featureType": "transit","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]}, {"featureType": "water","elementType": "geometry","stylers": [{"color": "#000000"}]}, {"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#3d3d3d"}]}
    ]
  }
}
