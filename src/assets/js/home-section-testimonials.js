$(function () {

  /**
   * ScrollMagic
   */
  new ScrollMagic.Scene({
      triggerElement: "#testimonials"
    })
    .addTo(inflecto.controller)
    .on("enter", function (e) {
      $("#testimonials").removeClass("section-initial");

      setTimeout(function () {
        $("#testimonials-slider").slick("slickPlay");
      }, 500);
    });


  /**
   * Slick slider
   */
  $("#testimonials-slider").slick({
    infinite: true,
    dots: true,
    arrows: false,
    mobileFirst: true,
    autoplay: false,
    autoplaySpeed: 5000,
    swipe: false,
    pauseOnHover: false,
    pauseOnFocus: false
  });
});
