$(function () {

  /**
   * ScrollMagic
   */
  new ScrollMagic.Scene({
      triggerElement: "#how"
    })
    .addTo(inflecto.controller)
    .on("enter", function (e) {
      $("#how").removeClass("section-initial");

      setTimeout(function () {
        $("#how-slider").slick("slickPlay");
      }, 500);
    });


  /**
   * Slick slider
   */
  $("#how-slider").slick({
      infinite: true,
      dots: true,
      arrows: false,
      fade: true,
      speed: 750,
      mobileFirst: true,
      autoplay: false,
      autoplaySpeed: 3000,
      pauseOnHover: false,
      pauseOnFocus: false
    })
    .on("beforeChange", function(e, slick, currentSlide, nextSlide) {
      $("#how .highlightable").removeClass("highlight");
      $("#how .highlight-" + Number(nextSlide + 1)).addClass("highlight");
    });
});
