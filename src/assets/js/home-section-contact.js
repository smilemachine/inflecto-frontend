$(function () {

  /**
   * ScrollMagic
   */
  new ScrollMagic.Scene({
      triggerElement: "#contact"
    })
    .addTo(inflecto.controller)
    .on("enter", function (e) {
      $("#contact").removeClass("section-initial");
      $("#contact input[name='check']").val(1);

      $.get("time.php", function (response) {
        $("#contact input[name='start']").val(response);
      });
  	});


  /**
   * Slick slider
   */
  $("#contact-slider").slick({
      infinite: true,
      dots: false,
      arrows: true,
      mobileFirst: true,
      swipe: false
    })
    .on("beforeChange", function (e, s, currentSlide, nextSlide) {
      $("#contact .nav li a").removeClass("active");
      $("#contact .nav li:nth-of-type(" + Number(nextSlide + 1) + ") a").addClass("active");
    });

  /**
   * Custom slick slider nav
   */
  $("#contact .nav").on("click", "a", function (e) {
    e.preventDefault();
    $("#contact-slider").slick("slickGoTo", $(this).parent("li").index(), false);
  });


  /**
   * Contact form submission an alternate way to the standard jQueryValidation
   * plugin's method due to incompatibilities with other plugins on page
   */
  $("#contact-form").on("submit", function (e) {
    e.preventDefault();

    var contactForm = $(this);

    contactForm.validate({
      rules: {
        fname: "required",
        lname: "required",
        email: {
          required: true,
          email: true
        },
        company: "required",
        telephone: "required",
        message: "required"
      },
      messages: {
        fname: "We need your first name",
        lname: "We need your last name",
        email: "We need a valid email",
        company: "We need your company name",
        telephone: "We need your number",
        message: "Say something!"
      }
    });

    if (contactForm.valid()) {
      contactForm.removeClass("contact-send-error");
      contactForm.removeClass("contact-send-success");
      contactForm.find(".contact-message-error").html("");
      contactForm.find(".contact-message-error").html("");
      $.ajax({
        type: "post",
        url: "mail.php",
        data: contactForm.serialize(),
        dataType: "json",
        success: function(response) {
          if (response.success !== undefined && response.success === true) {
            contactForm.find("input, textarea").attr("disabled", "disabled");
            contactForm.find(".contact-message-success").html(response.message);
            contactForm.addClass("contact-send-success");
          } else {
            contactForm.find(".contact-message-error").html(response.message);
            contactForm.addClass("contact-send-error");
          }
        },
        error: function (response) {
          contactForm.find(".contact-message-error").html("There was a problem sending your message. Please try again.");
          contactForm.addClass("contact-send-error");
        }
      });
    }
  });
});


/**
 * Find us map
 */
var map;

function initMap() {
  window.map = new google.maps.Map(document.getElementById('map'), inflecto.mapConfig);

  var image = 'assets/img/map-marker.png';
  var marker = new google.maps.Marker({
    position: inflecto.mapConfig.center,
    map: window.map,
    icon: image
  });
}
