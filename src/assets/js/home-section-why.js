$(function () {

  /**
   * ScrollMagic
   */
  new ScrollMagic.Scene({
      triggerElement: "#why"
    })
    .addTo(inflecto.controller)
    .on("enter", function (e) {
      $("#why").removeClass("section-initial");

      setTimeout(function () {
        $("#why-slider").slick("slickPlay");
      }, 500);
    });


  /**
   * Slick slider
   */
  $("#why-slider").slick({
      infinite: true,
      dots: true,
      arrows: false,
      fade: true,
      mobileFirst: true,
      autoplay: false,
      autoplaySpeed: 4000,
      pauseOnHover: false,
      pauseOnFocus: false
    })
    .on("beforeChange", function(e, slick, currentSlide, nextSlide) {
      $("#why .highlightable").removeClass("highlight");
      $("#why .highlight-" + Number(nextSlide + 1)).addClass("highlight");
    });
});
