$(function () {
  var smallScreenBreakpoint = 991;

  /**
   * Set initial section heights, and trigger it again
   * a few times incase the download is a bit slow
   */
  inflecto.setSectionHeights();
  inflecto.setMainWidth();
  inflecto.setNavOpenHeights();

  setTimeout(function () {
    inflecto.setSectionHeights();
    inflecto.setMainWidth();
    inflecto.setNavOpenHeights();
  }, 1000);

  setTimeout(function () {
    inflecto.setSectionHeights();
    inflecto.setMainWidth();
    inflecto.setNavOpenHeights();
  }, 3000);


  /**
   *  Auto hide the nav-open for the privacy page
   */
  if ($(window).width() < smallScreenBreakpoint) {
    $("body").removeClass("nav-open");
  }

  /**
   * On resize event
   */
  $(window).resize(function () {
    inflecto.setElementColor($("#nav-toggle"));
    inflecto.setElementColor($("#get-in-touch"));
    inflecto.setElementColor($("#top-links"));
    inflecto.setElementColor($("#back-to-top"));
    inflecto.setBackToTopVisibility();

    // Only for larger devices
    if ($(window).width() > smallScreenBreakpoint) {
      inflecto.setSectionHeights();

      if ($("body").hasClass("privacy")) {
        $("body").addClass("nav-open");
      }
    }

    inflecto.setMainWidth();
    inflecto.setNavOpenHeights();
  });


  /**
   * On scroll event
   */
  $(window).scroll(function () {
    inflecto.setElementColor($("#nav-toggle"));
    inflecto.setElementColor($("#get-in-touch"));
    inflecto.setElementColor($("#top-links"));
    inflecto.setElementColor($("#back-to-top"));
    inflecto.setBackToTopVisibility();

    // Only on smaller devices
    if ($(window).width() < smallScreenBreakpoint) {
      inflecto.triggerSections();
    }
  });


  /**
   * When a user clicks a section nav link, we want to scroll to the section,
   * and not jump as per the default behaviour
   */
  $("a.section-nav[href^='#']").on("click", function (e) {
    var selector = $(this).attr("href");

    if ($(selector).length > 0) {
      e.preventDefault();

      if ($(this).closest("nav").length == 0) {
        inflecto.scrollToElement($(selector));
      } else {
        if (!$("body").hasClass("privacy")) {
          $("body").removeClass("nav-open");

          setTimeout(function () {
            inflecto.scrollToElement($(selector));
          }, 300);
        } else {
          inflecto.scrollToElement($(selector));
        }
      }

      if (window.history && window.history.pushState) {
        history.pushState("", document.title, selector);
      }
    }
  });


  /**
   * Toggle the menu
   */
  $("#nav-toggle").on("click", function (e) {
    e.preventDefault();
    $("body").toggleClass("nav-open");
  });
});
