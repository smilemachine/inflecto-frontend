$(function () {

  /**
   * ScrollMagic
   */
  new ScrollMagic.Scene({
      triggerElement: "#publishers"
    })
    .addTo(inflecto.controller)
    .on("enter", function (e) {
      $("#publishers").removeClass("section-initial");
    });
});
