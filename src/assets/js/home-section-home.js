$(function () {

  /**
   * Autoplay the video. Android is a bitch for this.
   * http://stackoverflow.com/questions/11758651/autostart-html5-video-using-android-4-browser
   */
  var myVideo = document.getElementById("video");

  myVideo.addEventListener("canplay", function() {
    myVideo.play();
  });

  myVideo.load();
  myVideo.play();


  /**
   * ScrollMagic
   */
  new ScrollMagic.Scene({
      triggerElement: "#home"
    })
    .setPin("#home")
    .addTo(inflecto.controller)
  	.on("enter", function (e) {
      $("#home").removeClass("section-initial");
  	});
});
