$(function () {

  /**
   * ScrollMagic
   */
  new ScrollMagic.Scene({
      triggerElement: "#who"
    })
    .addTo(inflecto.controller)
  	.on("enter", function (e) {
      $("#who").removeClass("section-initial");
      $("#who-slider").slick("slickPlay");
  	});


  /**
   * Slick slider
   */
  $("#who-slider").slick({
    infinite: true,
    dots: false,
    arrows: false,
    fade: true,
    mobileFirst: true,
    autoplay: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
    pauseOnFocus: false
  });
});
